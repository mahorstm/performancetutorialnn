# PerformanceTutorialNN

This repository supplies all the files needed for the performance study of an HLT MET trigger. Detailed explanations can be found under [here](https://twiki.cern.ch/twiki/bin/view/Atlas/METTriggerPerformanceStudyWorkflow).

